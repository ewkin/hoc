import React, {useMemo, useState} from 'react';
import Spinner from "../../components/UI/Spinner/Spinner";

const withLoader = (WithLoader, axios) => {
    return props => {
        const [loader, setLoader] = useState(false);


        useMemo(() => {
            return axios.interceptors.request.use(res => res, () => setLoader(true))
        }, []);

        useMemo(() => {
            return axios.interceptors.response.use(res => res, () => setLoader(false))
        }, []);


        const loaderBox = () => (
            loader ? <Spinner/> : null
        );

        return (
            <>
                {loaderBox}
                <WithLoader {...props}/>
            </>
        );

    };
};

export default withLoader;